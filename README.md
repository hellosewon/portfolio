#
# SEWON HONG #
Seoul, South Korea | [hellosewon@gmail.com](mailto:hellosewon@gmail.com)
* * *
## Resume
- [Resume PDF (English)](https://bitbucket.org/hellosewon/portfolio/raw/0d4f0e983cd3c4393c1c82fa1545a47855d58392/Resume_SewonHong_English.pdf)
- [Resume PDF (Korean)](https://bitbucket.org/hellosewon/portfolio/raw/0d4f0e983cd3c4393c1c82fa1545a47855d58392/Resume_SewonHong_Korean.pdf)

## Portfolio
- [Portfolio PDF (English)](https://bitbucket.org/hellosewon/portfolio/raw/0d4f0e983cd3c4393c1c82fa1545a47855d58392/Portfolio_SewonHong_English.pdf)
- [Portfolio PDF (Korean)](https://bitbucket.org/hellosewon/portfolio/raw/0d4f0e983cd3c4393c1c82fa1545a47855d58392/Portfolio_SewonHong_Korean.pdf)
#
* * *
## Work Experience
####
October 2018 - September 2020
:   **[Coinone Inc.](https://coinone.co.kr/){:target="_blank"}, Software Engineer** as Skilled Industrial Personnel (Alternative Military Service)

    - [Cross](https://crossenf.com/){:target="_blank"}: International remittance service
    - [Cross Board](https://crossboard.io/){:target="_blank"}: Simple community website

#
* * *
## Portfolio Preview
![image info](portfolio_preview/0_title.PNG)
#
#
#
#### 1. Conation
![image info](portfolio_preview/3_0_conation_project_overview.PNG)
#
![image info](portfolio_preview/3_1_conation.PNG)
#
![image info](portfolio_preview/3_2_conation.PNG)
#
![image info](portfolio_preview/3_3_conation.PNG)
#
#
#
#### 2. Vocat
![image info](portfolio_preview/2_0_vocat_project_overview.PNG)
#
![image info](portfolio_preview/2_1_vocat.PNG)
#
![image info](portfolio_preview/2_2_vocat.PNG)
#
![image info](portfolio_preview/2_3_vocat.PNG)
#
![image info](portfolio_preview/2_4_vocat.PNG)
#
![image info](portfolio_preview/2_5_vocat.PNG)
#
![image info](portfolio_preview/2_6_vocat.PNG)
#
![image info](portfolio_preview/2_7_vocat.PNG)
#
![image info](portfolio_preview/2_9_vocat.PNG)
#
#
#
#### 3. Talk on games
![image info](portfolio_preview/1_0_tog_project_overview.PNG)
#
![image info](portfolio_preview/1_1_tog.PNG)
#
![image info](portfolio_preview/1_2_tog.PNG)
#
![image info](portfolio_preview/1_3_tog.PNG)
#
#
#